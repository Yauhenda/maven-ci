package org.example.badcode;

public class badCode {


        public int a,b,c; // No encapsulation

        public void method1() // Non-descriptive method name
        {
            if(true) { // Always true condition
                System.out.println("True");
            }
        }

        //Missing spaces between operators and missing braces
        public int badFormatting(int x,int y){return x+y;}

        //Unused method
        private void unusedMethod() {
            // Do nothing
        }

        //Unused variable
        private String unusedString = "This is not used";

        //Shadowing field
        public void setA(int a) {
            a = a;
        }

        public static void main(String args[]){
            badCode obj = new badCode();
            obj.method1(); // Inefficient use
            System.out.println(obj.badFormatting(2,3)); // No space after comma
        }
    }

